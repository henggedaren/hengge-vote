===============
投票小程序
===============

开源的是前端模板，可以自己对接后端。

如需完整版私聊作者（付费的，白嫖勿加），可直接拿来上线使用。

## 介绍

基于Thinkphp6.0 + uniapp开发的投票小程序

## 线上地址

上线中~~

## 页面演示

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/1582d47c-d23a-41da-a40e-423ddfd6de53.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/7e52bbf4-86c2-4317-908e-14b33a205e6e.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/71457434-f5dd-45bc-adec-ec20a8ce6ab8.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/b1f702cb-3ee3-445a-ab00-111d73f6d841.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/9ff9da36-8cb0-49ca-b339-a6c4c2ff6b20.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/709788ea-50cd-4389-8ce6-520f37e126e2.png)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-9225cf9c-812a-460f-a2e6-59b295dc56e4/2f40d1e8-410b-4c69-8eb5-91e2185f53c9.png)
